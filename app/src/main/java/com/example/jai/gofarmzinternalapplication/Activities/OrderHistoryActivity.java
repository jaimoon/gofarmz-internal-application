package com.example.jai.gofarmzinternalapplication.Activities;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;


import com.example.jai.gofarmzinternalapplication.R;

import java.util.Calendar;

public class OrderHistoryActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView calender_from,calender_to,close_img;
    private int mYear, mMonth, mDay, mHour, mMinute;
    EditText edt_from,edt_to;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.orderhistory_activity);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        calender_to=findViewById(R.id.calender_to);
        calender_to.setOnClickListener(this);
        calender_from=findViewById(R.id.calender_from);
        calender_from.setOnClickListener(this);

        edt_to=findViewById(R.id.edt_to);
        edt_from=findViewById(R.id.edt_from);

        close_img=findViewById(R.id.close_img);
        close_img.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        if (v == calender_from){

            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

                            edt_from.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                        }
                    }, mYear, mMonth, mDay);
            datePickerDialog.show();

        }

        if (v == calender_to){

            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

                            edt_to.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                        }
                    }, mYear, mMonth, mDay);
            datePickerDialog.show();

        }

        if (v == close_img){
            finish();
        }

    }
}
