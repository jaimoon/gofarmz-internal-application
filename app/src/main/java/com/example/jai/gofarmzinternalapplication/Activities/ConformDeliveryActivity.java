package com.example.jai.gofarmzinternalapplication.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.example.jai.gofarmzinternalapplication.R;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class ConformDeliveryActivity extends AppCompatActivity implements View.OnClickListener {
    SweetAlertDialog sweetAlertDialog;
    ImageView close_img;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.conform_delivery);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        close_img=findViewById(R.id.close_img);
        close_img.setOnClickListener(this);
        sweetAlertDialog = new SweetAlertDialog(ConformDeliveryActivity.this, SweetAlertDialog.WARNING_TYPE);
        sweetAlertDialog.setTitleText("Are you delivered?");
        sweetAlertDialog.setContentText("This product will be delivered!");
        sweetAlertDialog.setCancelText("No");
        sweetAlertDialog.setConfirmText("Yes");
        sweetAlertDialog.showCancelButton(true);

        sweetAlertDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sDialog) {
                startActivity(new Intent(ConformDeliveryActivity.this,OrderCancelActivity.class));
            }
        });
        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {

                startActivity(new Intent(ConformDeliveryActivity.this,OrderDeliveredActivity.class));

            }
        });
        sweetAlertDialog.show();
    }

    @Override
    public void onClick(View v) {
        if (v == close_img){
            finish();
        }

    }
}
