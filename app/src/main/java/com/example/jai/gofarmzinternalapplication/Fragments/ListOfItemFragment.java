package com.example.jai.gofarmzinternalapplication.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.example.jai.gofarmzinternalapplication.Activities.ListOfCustomersActivity;
import com.example.jai.gofarmzinternalapplication.Activities.OrderHistoryActivity;
import com.example.jai.gofarmzinternalapplication.R;


public class ListOfItemFragment extends BottomSheetDialogFragment {
RelativeLayout relative_date,relative_customer,relative_item;
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.item_fragment, container, false);
        relative_item=v.findViewById(R.id.relative_item);
        relative_customer=v.findViewById(R.id.relative_customer);
        relative_date=v.findViewById(R.id.relative_date);

        relative_customer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getActivity(), ListOfCustomersActivity.class));

            }
        });

        relative_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), OrderHistoryActivity.class));

            }
        });


        return v;
    }


}
