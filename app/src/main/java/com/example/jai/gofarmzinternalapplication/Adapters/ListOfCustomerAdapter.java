package com.example.jai.gofarmzinternalapplication.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.example.jai.gofarmzinternalapplication.Activities.CustomerDetailsActivity;
import com.example.jai.gofarmzinternalapplication.Activities.CustomerProductDetailsActivity;
import com.example.jai.gofarmzinternalapplication.Activities.ListOfCustomersActivity;
import com.example.jai.gofarmzinternalapplication.Models.CustomerModel;
import com.example.jai.gofarmzinternalapplication.R;


import java.util.ArrayList;

public class ListOfCustomerAdapter extends RecyclerView.Adapter<ListOfCustomerAdapter.Holder> {
    ArrayList<CustomerModel> arrayList;
    Context context;

    public ListOfCustomerAdapter(ArrayList<CustomerModel> arrayList, ListOfCustomersActivity listOfCustomersActivity) {
        this.arrayList = arrayList;
        this.context = listOfCustomersActivity;
    }

    @NonNull
    @Override
    public ListOfCustomerAdapter.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_customers, viewGroup, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ListOfCustomerAdapter.Holder holder, int i) {

        holder.c_name.setText(arrayList.get(i).getName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, CustomerProductDetailsActivity.class));
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        TextView c_name;

        public Holder(@NonNull View itemView) {
            super(itemView);
            c_name = itemView.findViewById(R.id.c_name);
        }
    }
}
