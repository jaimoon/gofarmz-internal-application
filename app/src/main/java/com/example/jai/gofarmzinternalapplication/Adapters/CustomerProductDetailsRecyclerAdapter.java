package com.example.jai.gofarmzinternalapplication.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.example.jai.gofarmzinternalapplication.Models.ProductModel;
import com.example.jai.gofarmzinternalapplication.R;



import java.util.ArrayList;

public class CustomerProductDetailsRecyclerAdapter extends RecyclerView.Adapter<CustomerProductDetailsRecyclerAdapter.Holder> {
    ArrayList<ProductModel> arrayList;
    Context context;

    public CustomerProductDetailsRecyclerAdapter(ArrayList<ProductModel> arrayList, Context context) {
        this.arrayList=arrayList;
        this.context=context;
    }

    @NonNull
    @Override
    public CustomerProductDetailsRecyclerAdapter.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_customerproducts, viewGroup, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomerProductDetailsRecyclerAdapter.Holder holder, int i) {
        holder.p_name.setText(arrayList.get(i).getP_name());
        holder.p_kg.setText(arrayList.get(i).getP_kg());

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class Holder extends RecyclerView.ViewHolder{
        ImageView product_img;
        TextView p_name,p_kg;
        public Holder(@NonNull View itemView) {
            super(itemView);

            p_kg=itemView.findViewById(R.id.p_kg);
            p_name=itemView.findViewById(R.id.p_name);
            product_img=itemView.findViewById(R.id.product_img);
        }
    }
}
