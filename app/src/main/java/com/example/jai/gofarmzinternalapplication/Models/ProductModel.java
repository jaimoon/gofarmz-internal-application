package com.example.jai.gofarmzinternalapplication.Models;

public class ProductModel {
    int p_image;
    String p_name,p_kg;

    public ProductModel(int p_image, String p_name, String p_kg) {
        this.p_image = p_image;
        this.p_name = p_name;
        this.p_kg = p_kg;
    }

    public int getP_image() {
        return p_image;
    }

    public void setP_image(int p_image) {
        this.p_image = p_image;
    }

    public String getP_name() {
        return p_name;
    }

    public void setP_name(String p_name) {
        this.p_name = p_name;
    }

    public String getP_kg() {
        return p_kg;
    }

    public void setP_kg(String p_kg) {
        this.p_kg = p_kg;
    }
}
