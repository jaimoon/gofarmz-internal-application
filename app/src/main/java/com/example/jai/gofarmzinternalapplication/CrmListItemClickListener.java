package com.example.jai.gofarmzinternalapplication;

import android.view.View;

public interface CrmListItemClickListener
{
    void onItemClick(View v, int pos);
}
