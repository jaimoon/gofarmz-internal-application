package com.example.jai.gofarmzinternalapplication.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.example.jai.gofarmzinternalapplication.Activities.DeliveriesActivity;
import com.example.jai.gofarmzinternalapplication.Activities.HomeActivity;
import com.example.jai.gofarmzinternalapplication.Activities.ListOfItemsActivity;
import com.example.jai.gofarmzinternalapplication.Activities.SupportActivity;
import com.example.jai.gofarmzinternalapplication.CrmListItemClickListener;
import com.example.jai.gofarmzinternalapplication.Models.HomeCategoryModel;
import com.example.jai.gofarmzinternalapplication.R;

public class HomeRecyclerAdapter extends RecyclerView.Adapter<HomeRecyclerAdapter.Holder> {
    Context context;
    private HomeCategoryModel[] categoryData;

    public HomeRecyclerAdapter(HomeCategoryModel[] data, HomeActivity homeActivity) {
        this.context=homeActivity;
        this.categoryData=data;
    }

    @NonNull
    @Override
    public HomeRecyclerAdapter.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_categories, viewGroup, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeRecyclerAdapter.Holder holder, int i) {

        holder.category_title.setText(categoryData[i].getCateg_name());
        holder.category_title1.setText(categoryData[i].getName());
        holder.img_categroy.setImageResource(categoryData[i].getCateg_image());

        holder.setItemClickListener(new CrmListItemClickListener()
        {
            @Override
            public void onItemClick(View v, int pos)
            {

                if(pos==0)
                {
                    context.startActivity(new Intent(context, DeliveriesActivity.class));
                }
                else if(pos==1)
                {
                    context.startActivity(new Intent(context, ListOfItemsActivity.class));
                }
                else if(pos==2)
                {
                    context.startActivity(new Intent(context, SupportActivity.class));
                }

            }
        });


    }

    @Override
    public int getItemCount() {
        return categoryData.length;
    }

    class Holder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public ImageView img_categroy;
        public CardView frame_week;
        public TextView category_title,category_title1;
        CrmListItemClickListener crmListItemClickListener;

        public Holder(@NonNull View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            img_categroy = itemView.findViewById(R.id.img_categroy);
            category_title = itemView.findViewById(R.id.category_title);
            category_title1 = itemView.findViewById(R.id.category_title1);
            frame_week = itemView.findViewById(R.id.frame_week);
        }

        @Override
        public void onClick(View view) {
            this.crmListItemClickListener.onItemClick(view, getLayoutPosition());
        }

        public void setItemClickListener(CrmListItemClickListener ic) {
            this.crmListItemClickListener = ic;
        }
    }
}
