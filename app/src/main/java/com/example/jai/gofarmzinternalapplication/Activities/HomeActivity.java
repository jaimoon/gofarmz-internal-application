package com.example.jai.gofarmzinternalapplication.Activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.WindowManager;

import com.example.jai.gofarmzinternalapplication.Adapters.HomeRecyclerAdapter;
import com.example.jai.gofarmzinternalapplication.Models.HomeCategoryModel;
import com.example.jai.gofarmzinternalapplication.R;



public class HomeActivity extends AppCompatActivity {
    RecyclerView recycler_categories;
    HomeRecyclerAdapter homeRecyclerAdapter;
    GridLayoutManager gridLayoutManager;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_activity);

        recycler_categories=findViewById(R.id.recycler_categories);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        HomeCategoryModel[] data = new HomeCategoryModel[] {
                new HomeCategoryModel("Deliveries","Manage Deliveries", R.drawable.ic_calender),
                new HomeCategoryModel("Procurement","Manage Procurement", R.drawable.ic_leads),
                new HomeCategoryModel("support","Customer Support",R.drawable.ic_task),
        };
        homeRecyclerAdapter=new HomeRecyclerAdapter(data,HomeActivity.this);
        recycler_categories.setNestedScrollingEnabled(false);

        gridLayoutManager = new GridLayoutManager(HomeActivity.this, 2);
        recycler_categories.setLayoutManager(gridLayoutManager);
        recycler_categories.setAdapter(homeRecyclerAdapter);

    }
}
