package com.example.jai.gofarmzinternalapplication.Activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.example.jai.gofarmzinternalapplication.R;

public class OrderDeliveredActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView close_img;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.orderdelivered_activity);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        close_img=findViewById(R.id.close_img);
        close_img.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == close_img){
            finish();
        }

    }
}
