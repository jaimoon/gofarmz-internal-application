package com.example.jai.gofarmzinternalapplication.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.jai.gofarmzinternalapplication.Activities.CustomerDetailsActivity;
import com.example.jai.gofarmzinternalapplication.Activities.DeliveriesActivity;
import com.example.jai.gofarmzinternalapplication.Activities.ListOfCustomersActivity;
import com.example.jai.gofarmzinternalapplication.Activities.MapsActivity;
import com.example.jai.gofarmzinternalapplication.Models.DeliveriesModel;
import com.example.jai.gofarmzinternalapplication.R;


import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class DeliveriesRecyclerAdapter extends RecyclerView.Adapter<DeliveriesRecyclerAdapter.Holder> {
    ArrayList<DeliveriesModel> arrayList;
    Context context;
    public DeliveriesRecyclerAdapter(ArrayList<DeliveriesModel> arrayList, DeliveriesActivity deliveriesActivity) {
        this.arrayList=arrayList;
        this.context=deliveriesActivity;
    }

    @NonNull
    @Override
    public DeliveriesRecyclerAdapter.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_deliveries, viewGroup, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DeliveriesRecyclerAdapter.Holder holder, int i) {

        holder.name.setText(arrayList.get(i).getName());
        holder.price.setText(arrayList.get(i).getPrice());
        holder.distance.setText(arrayList.get(i).getDistance());
//        Picasso.with(context).load(arrayList.get(i).getImage()).into(holder.image);

        holder.txt_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, MapsActivity.class));
            }
        });

        holder.txt_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, CustomerDetailsActivity.class));

            }
        });

    }

    @Override
    public int getItemCount() {

        return arrayList.size();
    }

    class Holder extends RecyclerView.ViewHolder{
        TextView distance,price,name,txt_accept,txt_view;
        ImageView image;

        public Holder(@NonNull View itemView) {
            super(itemView);

            distance=itemView.findViewById(R.id.distance);
            price=itemView.findViewById(R.id.price);
            name=itemView.findViewById(R.id.name);

            image=itemView.findViewById(R.id.image);

            txt_view=itemView.findViewById(R.id.txt_view);
            txt_accept=itemView.findViewById(R.id.txt_accept);
        }
    }
}
