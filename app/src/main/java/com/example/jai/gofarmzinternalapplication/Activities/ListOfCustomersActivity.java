package com.example.jai.gofarmzinternalapplication.Activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.example.jai.gofarmzinternalapplication.Adapters.ListOfCustomerAdapter;
import com.example.jai.gofarmzinternalapplication.Models.CustomerModel;
import com.example.jai.gofarmzinternalapplication.R;



import java.util.ArrayList;

public class ListOfCustomersActivity extends AppCompatActivity implements View.OnClickListener {
    RecyclerView recycler_customers;
    RecyclerView.LayoutManager layoutManager;
    ArrayList<CustomerModel> arrayList=new ArrayList<>();
    ListOfCustomerAdapter listOfCustomerAdapter;
    ImageView close_img;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listofcustomers_activity);
        recycler_customers=findViewById(R.id.recycler_customers);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        close_img=findViewById(R.id.close_img);
        close_img.setOnClickListener(this);

        arrayList.add(new CustomerModel("Ganesh"));
        arrayList.add(new CustomerModel("Ganesh"));
        arrayList.add(new CustomerModel("Ganesh"));
        arrayList.add(new CustomerModel("Ganesh"));
        arrayList.add(new CustomerModel("Ganesh"));
        arrayList.add(new CustomerModel("Ganesh"));
        arrayList.add(new CustomerModel("Ganesh"));


        listOfCustomerAdapter=new ListOfCustomerAdapter(arrayList,ListOfCustomersActivity.this);
        layoutManager = new LinearLayoutManager(ListOfCustomersActivity.this);
        recycler_customers.setNestedScrollingEnabled(false);
        recycler_customers.setLayoutManager(layoutManager);
        recycler_customers.setAdapter(listOfCustomerAdapter);


    }

    @Override
    public void onClick(View v) {
        if (v == close_img){
            finish();
        }
    }
}
