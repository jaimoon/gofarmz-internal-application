package com.example.jai.gofarmzinternalapplication.Activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.example.jai.gofarmzinternalapplication.Adapters.CustomerProductDetailsRecyclerAdapter;
import com.example.jai.gofarmzinternalapplication.Fragments.ListOfItemFragment;
import com.example.jai.gofarmzinternalapplication.Models.ProductModel;
import com.example.jai.gofarmzinternalapplication.R;



import java.util.ArrayList;

public class ListOfItemsActivity extends AppCompatActivity implements View.OnClickListener{
    RecyclerView recycler_listitems;
    ArrayList<ProductModel> arrayList=new ArrayList<>();
    CustomerProductDetailsRecyclerAdapter customerProductDetailsRecyclerAdapter;
    RecyclerView.LayoutManager layoutManager;
    RelativeLayout relative_total;
    public static final String TAG = "bottom_sheet";
    public ListOfItemFragment listOfItemFragment;
    ImageView close_img;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listofitems_activity);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        close_img=findViewById(R.id.close_img);
        close_img.setOnClickListener(this);
        relative_total=findViewById(R.id.relative_total);
        relative_total.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listOfItemFragment=new ListOfItemFragment();
                listOfItemFragment.show(getSupportFragmentManager(), TAG);
            }
        });

        recycler_listitems=findViewById(R.id.recycler_listitems);

        arrayList.add(new ProductModel(R.drawable.ic_launcher_background,"tomato","1kg"));
        arrayList.add(new ProductModel(R.drawable.ic_launcher_background,"tomato","1kg"));
        arrayList.add(new ProductModel(R.drawable.ic_launcher_background,"tomato","1kg"));
        arrayList.add(new ProductModel(R.drawable.ic_launcher_background,"tomato","1kg"));
        arrayList.add(new ProductModel(R.drawable.ic_launcher_background,"tomato","1kg"));
        arrayList.add(new ProductModel(R.drawable.ic_launcher_background,"tomato","1kg"));
        customerProductDetailsRecyclerAdapter=new CustomerProductDetailsRecyclerAdapter(arrayList, ListOfItemsActivity.this);
        layoutManager=new LinearLayoutManager(ListOfItemsActivity.this);
        recycler_listitems.setNestedScrollingEnabled(false);
        recycler_listitems.setLayoutManager(layoutManager);
        recycler_listitems.setAdapter(customerProductDetailsRecyclerAdapter);
    }

    @Override
    public void onClick(View v) {
        if (v == close_img){
            finish();
        }
    }
}
