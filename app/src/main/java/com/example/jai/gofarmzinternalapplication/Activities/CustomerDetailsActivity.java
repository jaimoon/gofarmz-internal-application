
 package com.example.jai.gofarmzinternalapplication.Activities;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.example.jai.gofarmzinternalapplication.Adapters.CustomerProductDetailsRecyclerAdapter;
import com.example.jai.gofarmzinternalapplication.Models.ProductModel;
import com.example.jai.gofarmzinternalapplication.R;


import java.util.ArrayList;

public class CustomerDetailsActivity extends AppCompatActivity implements View.OnClickListener {
    ArrayList<ProductModel> arrayList=new ArrayList<>();
    CustomerProductDetailsRecyclerAdapter customerProductDetailsRecyclerAdapter;
    RecyclerView recycler_products;
    RecyclerView.LayoutManager layoutManager;
    CardView cardView;
    ImageView close_img;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.customerdetails_activity);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        close_img=findViewById(R.id.close_img);
        close_img.setOnClickListener(this);
        recycler_products=findViewById(R.id.recycler_products);
        cardView=findViewById(R.id.cardView);
//        cardView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(CustomerDetailsActivity.this, ListOfItemsActivity.class));
//            }
//        });


        arrayList.add(new ProductModel(R.drawable.ic_launcher_background,"tomato","1kg"));
        arrayList.add(new ProductModel(R.drawable.ic_launcher_background,"tomato","1kg"));
        arrayList.add(new ProductModel(R.drawable.ic_launcher_background,"tomato","1kg"));
        arrayList.add(new ProductModel(R.drawable.ic_launcher_background,"tomato","1kg"));
        arrayList.add(new ProductModel(R.drawable.ic_launcher_background,"tomato","1kg"));
        arrayList.add(new ProductModel(R.drawable.ic_launcher_background,"tomato","1kg"));
        customerProductDetailsRecyclerAdapter=new CustomerProductDetailsRecyclerAdapter(arrayList,CustomerDetailsActivity.this);
        layoutManager=new LinearLayoutManager(CustomerDetailsActivity.this);
        recycler_products.setNestedScrollingEnabled(false);
        recycler_products.setLayoutManager(layoutManager);
        recycler_products.setAdapter(customerProductDetailsRecyclerAdapter);

    }

    @Override
    public void onClick(View v) {
        if (v == close_img){
            finish();
        }

    }
}
