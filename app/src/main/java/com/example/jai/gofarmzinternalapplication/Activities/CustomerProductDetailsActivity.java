package com.example.jai.gofarmzinternalapplication.Activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.example.jai.gofarmzinternalapplication.Adapters.CustomerProductDetailsRecyclerAdapter;
import com.example.jai.gofarmzinternalapplication.Models.ProductModel;
import com.example.jai.gofarmzinternalapplication.R;

import java.util.ArrayList;


public class CustomerProductDetailsActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView close_img;
    RecyclerView recycler_customerDetails;
    ArrayList<ProductModel> arrayList=new ArrayList<>();
    CustomerProductDetailsRecyclerAdapter customerProductDetailsRecyclerAdapter;
    RecyclerView.LayoutManager layoutManager;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.customerproductdetails_activity);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        close_img=findViewById(R.id.close_img);
        close_img.setOnClickListener(this);

        recycler_customerDetails=findViewById(R.id.recycler_customerDetails);

        arrayList.add(new ProductModel(R.drawable.ic_launcher_background,"tomato","1kg"));
        arrayList.add(new ProductModel(R.drawable.ic_launcher_background,"tomato","1kg"));
        arrayList.add(new ProductModel(R.drawable.ic_launcher_background,"tomato","1kg"));
        arrayList.add(new ProductModel(R.drawable.ic_launcher_background,"tomato","1kg"));
        arrayList.add(new ProductModel(R.drawable.ic_launcher_background,"tomato","1kg"));
        arrayList.add(new ProductModel(R.drawable.ic_launcher_background,"tomato","1kg"));
        customerProductDetailsRecyclerAdapter=new CustomerProductDetailsRecyclerAdapter(arrayList, CustomerProductDetailsActivity.this);
        layoutManager=new LinearLayoutManager(CustomerProductDetailsActivity.this);
        recycler_customerDetails.setNestedScrollingEnabled(false);
        recycler_customerDetails.setLayoutManager(layoutManager);
        recycler_customerDetails.setAdapter(customerProductDetailsRecyclerAdapter);
    }

    @Override
    public void onClick(View v) {
        if (v == close_img){
            finish();
        }

    }
}
