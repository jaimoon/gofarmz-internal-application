package com.example.jai.gofarmzinternalapplication.Models;

public class DeliveriesModel {
    int image;
    String name,price,distance;

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public DeliveriesModel(int image, String name, String price,String distance) {
        this.image = image;
        this.name = name;
        this.price = price;
        this.distance=distance;
    }
}
