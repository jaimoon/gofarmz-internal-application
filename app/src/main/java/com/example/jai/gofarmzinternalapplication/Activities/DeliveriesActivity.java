package com.example.jai.gofarmzinternalapplication.Activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.example.jai.gofarmzinternalapplication.Adapters.DeliveriesRecyclerAdapter;
import com.example.jai.gofarmzinternalapplication.Models.DeliveriesModel;
import com.example.jai.gofarmzinternalapplication.R;



import java.util.ArrayList;

public class DeliveriesActivity extends AppCompatActivity implements View.OnClickListener {
    ArrayList<DeliveriesModel> arrayList=new ArrayList<>();
    RecyclerView recycler_deliveries;
    RecyclerView.LayoutManager layoutManager;
    DeliveriesRecyclerAdapter deliveriesRecyclerAdapter;
    ImageView close_img;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.deliveries_activity);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        close_img=findViewById(R.id.close_img);
        close_img.setOnClickListener(this);
        recycler_deliveries=findViewById(R.id.recycler_deliveries);

        arrayList.add(new DeliveriesModel(R.drawable.ic_calender,"ganesh","320","320"));
        arrayList.add(new DeliveriesModel(R.drawable.ic_calender,"ganesh","320","320"));
        arrayList.add(new DeliveriesModel(R.drawable.ic_calender,"ganesh","320","320"));
        arrayList.add(new DeliveriesModel(R.drawable.ic_calender,"ganesh","320","320"));
        arrayList.add(new DeliveriesModel(R.drawable.ic_calender,"ganesh","320","320"));
        arrayList.add(new DeliveriesModel(R.drawable.ic_calender,"ganesh","320","320"));

        deliveriesRecyclerAdapter=new DeliveriesRecyclerAdapter(arrayList,DeliveriesActivity.this);
        layoutManager = new LinearLayoutManager(DeliveriesActivity.this);
        recycler_deliveries.setNestedScrollingEnabled(false);
        recycler_deliveries.setLayoutManager(layoutManager);
        recycler_deliveries.setAdapter(deliveriesRecyclerAdapter);


    }

    @Override
    public void onClick(View v) {

        if (v == close_img){
            finish();
        }
    }
}
