package com.example.jai.gofarmzinternalapplication.Models;

public class CustomerModel {
    String name;

    public CustomerModel(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
